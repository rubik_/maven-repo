## Setup
```
// clone
git clone git@bitbucket.org:rubik_/maven-repo.git
```

## Install Jar
```
// Change dir
cd maven-repo

// install jar
// example command, replace file, groupId, artifactId and version accordingly.
mvn install:install-file \
  -Dfile=/Users/animesh/Downloads/avro-1.7.4.jar \
  -DgroupId=org.apache.avro \
  -DartifactId=avro \
  -Dversion=1.7.4 \
  -Dpackaging=jar \
  -DgeneratePom=true \
  -Dmaven.repo.local=$(pwd)/maven2
```

## Repo

```
<repositories>
    <repository>
        <id>rubik-internal-repo</id>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <url>http://192.168.1.12:31008/maven2/</url>
    </repository>
</repositories>
```
